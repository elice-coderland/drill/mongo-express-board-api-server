const { Schema } = require('mongoose');

const PostSchema = new Schema({
        author: String,
        title: String,
        content: String,
}, {
    timestamps: true,
});

module.exports = PostSchema;