const { Post } = require('../models');

exports.getAllPosts = async (req, res, next) => {
    try {
        const allPosts = await Post.find();
        res.send(allPosts);
    } catch (err) {
        next(err);
    }
}

exports.getPostsByAuthor = async (req, res, next) => {
    const { author } = req.params;
    try {
        const postsByAuthor = await Post.find({ author });
        res.send(postsByAuthor)
    } catch (err) {
        next(err);
    }
}

exports.createPost = async (req, res, next) => {
    const { author, title, content } = req.body;
    try {
        const newPost = await Post.create({ author, title, content });
        res.send(newPost)
    } catch (err) {
        next(err);
    }
}

exports.updatePost = async (req, res, next) => {
    const { author } = req.params;
    const { title, content } = req.body;
    try {
        const result = await Post.updateOne({ author }, { title, content });
        res.send(`${result.modifiedCount} affected.`);
    } catch (err) {
        next(err);
    }
}

exports.deletePost = async (req, res, next) => {
    const { author } = req.params;
    try {
        const result = await Post.deleteOne({ author });
        res.send(`${result.deletedCount} deleted`);
    } catch (err) {
        next(err);
    }
}
