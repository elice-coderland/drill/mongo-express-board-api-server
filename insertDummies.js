const dummyData = [
    { author: "JohnDoe", title: "First post", content: "Lorem ipsum dolor sit amet." },
    { author: "JaneSmith", title: "Second post", content: "Sed ut perspiciatis unde omnis iste natus error." },
    { author: "BobJohnson", title: "Third post", content: "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet." }
  ];
  
  Post.insertMany(dummyData)
  .then(docs => {
    console.log(`${docs.length} dummy posts inserted`);
  })
  .catch(err => {
    console.error(err);
  });
