const { Router } = require('express');
const router = Router();
const { getAllPosts, getPostsByAuthor, createPost, updatePost, deletePost } = require('../controllers/PostsController');

router.get('/', getAllPosts);
router.get('/:author', getPostsByAuthor);
router.post('/', createPost);
router.put('/:author', updatePost);
router.delete('/:author', deletePost);

module.exports = router;

