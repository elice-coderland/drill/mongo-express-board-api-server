const express = require('express');
const app = express();
const mongoose = require('mongoose');
const postsRouters = require('./routes/postsRouter');

mongoose.connect('mongodb+srv://');

app.use(express.json());

app.use((err, req, res, next) => {
    console.err(err.stack);
    res.status(500).sned({
        error: 'Something wrong!'
    })
})

app.use('/posts', postsRouters);

app.listen(8080, () => {
    console.log('The Server is Running at PORT 8080');
})